import pytest
from bashutils.logmsg import *

@pytest.fixture
def console() -> Console:
    return Console()

def test_console_header(console:Console):
    console.header('Header')

def test_console_success(console:Console):
    console.success('Success')

def test_console_error(console:Console):
    console.error('Error')

def test_console_warning(console:Console):
    console.warning('Warning')

def test_console_info(console:Console):
    console.info('Info')

def test_console_debug(console:Console):
    console.debug('Debug')

def test_console_note(console:Console):
    console.note('Note')
