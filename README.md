# Python Bash Utils
Author: Tim Santor <tsantor@xstudios.com>

# Overview
Bash color management and log system for Python users.


# Requirements

- Python 3.8.x
# Installation

You can install directly via pip:

    pip install python-bash-utils

# Usage

## logmsg

Import:

    from bashutils import logmsg

Functions:

    logmsg.divline()             # ----------
    logmsg.header('header')      # ==> header
    logmsg.success('success')    # [OK] success
    logmsg.error('error')        # [ERROR] error
    logmsg.warning('warning')    # [WARNING] warning
    logmsg.info('info')          # [INFO] info
    logmsg.note('note')          # note

    logmsg.prompt('What is your name?)  # [?] What is your name?
    lomgsg.confirm('Confirm this')  # [?] Confirm this? (y/n)


## bashutils

Import:

    from bashutils import bashutils

Functions:

    bashutils.get_os() # OSX, 'Fedora', 'CentOS', 'Debian', 'Ubuntu'

    status, stdout, stderr = bashutils.exec_cmd('git -h')

    bashutils.cmd_exists('git') # True or False

# Issues

If you experience any issues, please create an [issue](https://bitbucket.org/tsantor/python-bash-utils/issues) on Bitbucket.
