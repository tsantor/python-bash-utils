# Version History

- **0.1.0** - Initial release
- **0.1.1** - Added some new methods
- **0.1.2** - Refactor
- **0.1.3** - Added Python 3.4.x support
- **0.1.4** - Added ability to pass logger instance to log message as well
- **0.1.5** - Added Windows output workaround
- **0.1.6** - Removed needless `declined` method, was being overly verbose in output
- **0.2.0** - Updated for Python 3
- **0.3.0** - Simplified code base and now rely on `colorama` for cross-platform terminal color support
- **0.3.1** - Added cooler icons for success/error for Linux/Mac
- **0.3.2** - Remove unicode chars that cause issues when writing to file
- **0.3.3** - Updated packaging method, updated setup.py
